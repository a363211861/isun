//
//  CellForTextField.m
//  IOSSun
//
//  Created by Y W on 13-6-8.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "CellForTextField.h"

@implementation CellForTextField

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        {
            CGRect rect = self.bounds;
            rect.size.height = [CellForTextField cellHeight];
            self.bounds = rect;
            
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            self.accessoryType = UITableViewCellAccessoryNone;
        }
        
        {
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 5, self.contentView.bounds.size.width - 10 * 2, self.contentView.bounds.size.height - 5 * 2)];
            textField.backgroundColor = [UIColor clearColor];
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.returnKeyType = UIReturnKeyDone;
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [self.contentView addSubview:textField];
            _textField = textField;
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (float)cellHeight
{
    return 40;
}

@end
