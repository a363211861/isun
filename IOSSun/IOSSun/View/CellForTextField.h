//
//  CellForTextField.h
//  IOSSun
//
//  Created by Y W on 13-6-8.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForTextField : UITableViewCell

@property (nonatomic, readonly)UITextField *textField;

+ (float)cellHeight;

@end
