//
//  Networking.m
//  IOSSun
//
//  Created by Y W on 13-3-20.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Networking.h"

#import "NetworkingClient.h"
#import "AFHTTPRequestOperation.h"
#import "NSData+CommonCrypto.h"
#import "NSData+Base64.h"
#import "Util.h"
#import "b64.h"

@interface Networking ()
{
    __strong NSError *_error;
    __strong id _responseObject;
    __strong AFHTTPRequestOperation *_httpRequestOperation;
    
    NSTimer *_timer;
}

@property(nonatomic, strong)id AES256Key;

//在这个方法里面添加公用到的param
- (NSMutableDictionary *)checkParameters:(NSDictionary *)parameters;

//在这个方法里判断是否有自定义的错误
- (BOOL)checkCustomError:(id)JSON;

- (void)cancel;

//delegate
- (void)success;
- (void)fail;

@end


@implementation Networking

@synthesize error = _error;
@synthesize responseObject = _responseObject;

- (id)init
{
    self = [super init];
    if (self) {
        self.timeoutSecs = 30;
        Byte keyByte[] = {0x08, 0x08, 0x04, 0x0b, 0x02, 0x0f, 0x0b, 0x0c,
            0x01, 0x03, 0x09, 0x07, 0x0c, 0x03, 0x07, 0x0a, 0x04, 0x0f,
            0x06, 0x0f, 0x0e, 0x09, 0x05, 0x01, 0x0a, 0x0a, 0x01, 0x09,
            0x06, 0x07, 0x09, 0x0d};
        self.AES256Key = [[NSData alloc] initWithBytes:keyByte length:32];//@"884b2fbc1397c37a4f6fe951aa19679d"///[Util md5StringForString:@"kdsdsgcdk/aslkiunfg/poinjglaw/ew"];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startNotication:) name:AFNetworkingOperationDidStartNotification object:nil];
    }
    return self;
}


- (void)dealloc
{
    [self cancel];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)startNotication:(NSNotification *)notification
{
    id obj = [notification object];
    if (obj == _httpRequestOperation) {
        [self startTimer];
    }
}

- (void)startTimer
{
    [self stopTimer];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:self.timeoutSecs target:self selector:@selector(timeOut) userInfo:nil repeats:NO];
}

- (void)stopTimer
{
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)timeOut
{
    [self cancel];
    _error = [Util errorWithLocalizedDescription:@"连接超时"];
}


//加密
- (NSDictionary *)AES256Encryption:(NSDictionary *)parameters
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    NSData *aseData = [data AES256EncryptedDataUsingKey:self.AES256Key error:nil];
    NSString *base64EncodedString = [aseData base64EncodedString];
    NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSLog(@"\n   %@", base64EncodedString);
    
    if (base64EncodedString == nil) {
        return nil;
    }
    
    return [NSDictionary dictionaryWithObjectsAndKeys:base64EncodedString, @"parameters", nil];
}


//解密
- (NSDictionary *)AES256Decryption:(NSData *)responseData
{
    //NSString *base64EncodedString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    //NSData *base64Data = [NSData dataFromBase64String:base64EncodedString];
    NSData *base64Data = b64_decode(responseData);
    NSData *deData = [base64Data decryptedAES256DataUsingKey:self.AES256Key error:nil];
    
    NSLog(@"%@", [[NSString alloc] initWithData:deData encoding:NSUTF8StringEncoding]);
    
    if (deData == nil) {
        return nil;
    }
    
    return [NSJSONSerialization JSONObjectWithData:deData options:NSJSONReadingAllowFragments error:nil];
}


//在这个方法里面添加公用到的param
- (NSMutableDictionary *)checkParameters:(NSDictionary *)parameters
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    
    return params;
}


//这个方法检查返回的数据正不正确
- (BOOL)checkCustomError:(id)JSON
{
    if ([JSON isKindOfClass:[NSData class]]) {
        JSON = [self AES256Decryption:JSON];
        _responseObject = JSON;
    }
    if (JSON == nil || ![JSON respondsToSelector:@selector(objectForKey:)]) {
        return YES;
    }
    
    id resultCode = [JSON objectForKey:@"resultCode"];
    if (resultCode && [resultCode respondsToSelector:@selector(intValue)]) {
        int code = [resultCode intValue];
        switch (code) {
            case 0:
            {
                return NO; //正常
            }
                break;
            default:
            {
                _error = [Util errorWithLocalizedDescription:[JSON objectForKey:@"resultDescription"]];
                return YES;
            }
                break;
        }
    }
    return YES;
}


- (void)cancel
{
    if (_httpRequestOperation != nil && !_httpRequestOperation.isFinished) {
        [_httpRequestOperation cancel];
    }
    _httpRequestOperation = nil;
}


// call delegate
- (void)success
{
    [self stopTimer];
    if (self.delegate && [self.delegate respondsToSelector:@selector(networkingSuccess:)]) {
        [self.delegate networkingSuccess:self];
    }
}


- (void)fail
{
    [self stopTimer];
    if (self.delegate && [self.delegate respondsToSelector:@selector(networkingFail:)]) {
        [self.delegate networkingFail:self];
    }
}
@end




@implementation Networking (GetAndPost)

- (id)initGetParameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate
{
    self = [self init];
    if (self) {
        self.delegate = delegate;
        if (!parameters || [parameters isKindOfClass:[NSDictionary class]]) {
            __weak Networking *weakSelf = self;
            _httpRequestOperation = [[NetworkingClient sharedClient] getPath:nil parameters:[self AES256Encryption:parameters] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([weakSelf checkCustomError:responseObject]) {
                    [weakSelf fail];
                } else {
                    [weakSelf success];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                _error = error;
                [weakSelf fail];
            }];
        }
    }
    return self;
}

- (id)initPostParameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate;
{
    self = [self init];
    if (self) {
        self.delegate = delegate;
        if (!parameters || [parameters isKindOfClass:[NSDictionary class]]) {
            __weak Networking *weakSelf = self;
            _httpRequestOperation = [[NetworkingClient sharedClient] postPath:nil parameters:[self AES256Encryption:parameters] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([weakSelf checkCustomError:responseObject]) {
                    [weakSelf fail];
                } else {
                    [weakSelf success];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                _error = error;
                [weakSelf fail];
            }];
        }
    }
    return self;
}

@end



@implementation Networking (Upload)

- (id)initPostUploadDataArray:(NSArray *)uploadDataArray parameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate
{
    self = [self init];
    if (self) {
        self.delegate = delegate;
        if (uploadDataArray && [uploadDataArray isKindOfClass:[NSArray class]] && (!parameters || [parameters isKindOfClass:[NSDictionary class]])) {
            __weak Networking *weakSelf = self;
            _httpRequestOperation = [[NetworkingClient sharedUploadClient] postUploadPath:nil dataArray:uploadDataArray parameters:[self AES256Encryption:parameters] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([weakSelf checkCustomError:responseObject]) {
                    [weakSelf fail];
                } else {
                    [weakSelf success];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                _error = error;
                [weakSelf fail];
            }];
            
            [_httpRequestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(networking:progress:)]) {
                    [weakSelf.delegate networking:weakSelf progress:floorf(totalBytesWritten/(totalBytesExpectedToWrite * 1.0))];
                }
            }];
        }
    }
    return self;
}

@end





@implementation Networking (Download)

- (id)initGetDownloadParameters:(NSDictionary *)parameters downloadFileSavePath:(NSString *)filePath append:(BOOL)append delegate:(id<NetworkingDelegate>)delegate
{
    self = [self init];
    if (self) {
        self.delegate = delegate;
        if ((!parameters || [parameters isKindOfClass:[NSDictionary class]]) && filePath && [filePath isKindOfClass:[NSString class]]) {
            __weak Networking *weakSelf = self;
            _httpRequestOperation = [[NetworkingClient sharedDownloadClient] getDownloadPath:nil filePath:filePath append:append parameters:[self AES256Encryption:parameters] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([weakSelf checkCustomError:responseObject]) {
                    [weakSelf fail];
                } else {
                    [weakSelf success];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                _error = error;
                [weakSelf fail];
            }];
            
            [_httpRequestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(networking:progress:)]) {
                    [weakSelf.delegate networking:weakSelf progress:floorf(totalBytesRead/(totalBytesExpectedToRead * 1.0))];
                }
            }];
        }
    }
    return self;
}

- (id)initPostDownloadParameters:(NSDictionary *)parameters downloadFileSavePath:(NSString *)filePath append:(BOOL)append delegate:(id<NetworkingDelegate>)delegate
{
    self = [self init];
    if (self) {
        self.delegate = delegate;
        if ((!parameters && [parameters isKindOfClass:[NSDictionary class]]) && filePath && [filePath isKindOfClass:[NSString class]]) {
            __weak Networking *weakSelf = self;
            _httpRequestOperation = [[NetworkingClient sharedDownloadClient] postDownloadPath:nil filePath:filePath append:append parameters:[self AES256Encryption:parameters] success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if ([weakSelf checkCustomError:responseObject]) {
                    [weakSelf fail];
                } else {
                    [weakSelf success];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                _error = error;
                [weakSelf fail];
            }];
            
            [_httpRequestOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
                if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(networking:progress:)]) {
                    [weakSelf.delegate networking:weakSelf progress:floorf(totalBytesRead/(totalBytesExpectedToRead * 1.0))];
                }
            }];
        }
    }
    return self;
}

@end
