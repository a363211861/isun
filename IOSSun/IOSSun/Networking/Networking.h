//
//  Networking.h
//  IOSSun
//
//  Created by Y W on 13-3-20.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

@class Networking;
@protocol NetworkingDelegate <NSObject>

@optional
-(void)networkingSuccess:(Networking *)networking;
-(void)networkingFail:(Networking *)networking;
-(void)networking:(Networking *)networking progress:(float)progress;

@end


@interface Networking : NSObject

@property(nonatomic, weak) id<NetworkingDelegate> delegate;
@property(nonatomic, assign) NSUInteger timeoutSecs; //默认30秒
@property(nonatomic, readonly) NSError *error;
@property(nonatomic, readonly) id responseObject;

@end


@interface Networking (GetAndPost)

-(id)initGetParameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate;
-(id)initPostParameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate;

@end



@interface Networking (Upload)

-(id)initPostUploadDataArray:(NSArray *)uploadDataArray parameters:(NSDictionary *)parameters delegate:(id<NetworkingDelegate>)delegate;

@end



@interface Networking (Download)

-(id)initGetDownloadParameters:(NSDictionary *)parameters downloadFileSavePath:(NSString *)filePath append:(BOOL)append delegate:(id<NetworkingDelegate>)delegate;

-(id)initPostDownloadParameters:(NSDictionary *)parameters downloadFileSavePath:(NSString *)filePath append:(BOOL)append delegate:(id<NetworkingDelegate>)delegate;

@end