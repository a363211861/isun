//
//  NetworkingClient.m
//  IOSSun
//
//  Created by Y W on 13-3-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "NetworkingClient.h"

#import "AFJSONRequestOperation.h"
#import "FileUtil.h"

#ifdef __i386__

#define kServerAddress @"http://localhost:8080/ISunServer/iSunDefaultAction"

#else

#define kServerAddress @"http://192.168.1.2:8080/ISunServer/iSunDefaultAction"
//#define kServerAddress @"http://169.254.189.56:8080/ISunServer/iSunDefaultAction"

#endif

#define kServerURL_Request  [NSURL URLWithString:kServerAddress]
#define kServerURL_Upload   [NSURL URLWithString:kServerAddress]
#define kServerURL_Download [NSURL URLWithString:kServerAddress]


@implementation NetworkingClient

//单例模式

+ (NetworkingClient *)sharedClient
{
    static NetworkingClient *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[NetworkingClient alloc] initWithBaseURL:kServerURL_Request];
        [sharedClient.operationQueue setMaxConcurrentOperationCount:4];
    });
    return sharedClient;
}

+ (NetworkingClient *)sharedUploadClient
{
    static NetworkingClient *sharedUploadClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUploadClient = [[NetworkingClient alloc] initWithBaseURL:kServerURL_Upload];
        [sharedUploadClient.operationQueue setMaxConcurrentOperationCount:4];
    });
    return sharedUploadClient;
}

+ (NetworkingClient *)sharedDownloadClient
{
    static NetworkingClient *sharedDownloadClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDownloadClient = [[NetworkingClient alloc] initWithBaseURL:kServerURL_Download];
        [sharedDownloadClient.operationQueue setMaxConcurrentOperationCount:4];
    });
    return sharedDownloadClient;
}

//继承

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        NSDictionary *headerParamters = [self headerParamters];
        if (headerParamters && headerParamters.count > 0) {
            NSArray *keys = [headerParamters allKeys];
            for (id key in keys) {
                [self setDefaultHeader:key value:[headerParamters objectForKey:key]];
            }
        }
    }
    
    return self;
}

//重写

-(AFHTTPRequestOperation *)getPath:(NSString *)path
                        parameters:(NSDictionary *)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURLRequest *request = [self requestWithMethod:@"GET" path:path parameters:parameters];
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self enqueueHTTPRequestOperation:operation];
    
    return operation;
}


-(AFHTTPRequestOperation *)postPath:(NSString *)path
                         parameters:(NSDictionary *)parameters
                            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURLRequest *request = [self requestWithMethod:@"POST" path:path parameters:parameters];
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self enqueueHTTPRequestOperation:operation];
    
    return operation;
}


-(AFHTTPRequestOperation *)postUploadPath:(NSString *)path
                                dataArray:(NSArray *)dataArray
                               parameters:(NSDictionary *)parameters
                                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURLRequest *request = [self multipartFormRequestWithMethod:@"POST" path:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for (id dic in dataArray) {
            if ([dic respondsToSelector:@selector(objectForKey:)]) {
                id data = [dic objectForKey:@"data"];
                id name = [dic objectForKey:@"name"];
                id fileName = [dic objectForKey:@"fileName"];
                id mimeType = [dic objectForKey:@"mimeType"];
                if (data && name && [name isKindOfClass:[NSString class]]) {
                    if ([data isKindOfClass:[NSString class]]) {
                        NSURL *fileUrl = [NSURL fileURLWithPath:data];
                        NSError *error;
                        if (fileUrl) {
                            if ([formData appendPartWithFileURL:fileUrl name:name error:&error]) {
                                NSLog(@"上传文件时处理成功！");
                            } else {
                                NSLog(@"上传文件失败！ error:%@", error);
                            }
                        }
                    } else if ([data isKindOfClass:[NSData class]] && fileName && [fileName isKindOfClass:[NSString class]] && mimeType && [mimeType isKindOfClass:[NSString class]]) {
                        [formData appendPartWithFileData:data name:name fileName:fileName mimeType:mimeType];
                    } else {
                        assert(0);
                    }
                } else {
                    assert(0);
                }
            }
        }
    }];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self enqueueHTTPRequestOperation:operation];
    
    return operation;
}


-(AFHTTPRequestOperation *)getDownloadPath:(NSString *)path
                                  filePath:(NSString *)filePath
                                    append:(BOOL)append
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self requestWithMethod:@"GET" path:path parameters:parameters];
    if (append) {
        [request setValue:[NSString stringWithFormat:@"bytes=%llu-", [[FileUtil sharedFileUtil] fileSizeForPath:filePath]] forHTTPHeaderField:@"Range"];
    }
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    [self enqueueHTTPRequestOperation:operation];
    
    return operation;
}

-(AFHTTPRequestOperation *)postDownloadPath:(NSString *)path
                                   filePath:(NSString *)filePath
                                     append:(BOOL)append
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSMutableURLRequest *request = [self requestWithMethod:@"POST" path:path parameters:parameters];
    if (append) {
        [request setValue:[NSString stringWithFormat:@"bytes=%llu-", [[FileUtil sharedFileUtil] fileSizeForPath:filePath]] forHTTPHeaderField:@"Range"];
    }
	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:append];
    [self enqueueHTTPRequestOperation:operation];
    
    return operation;
}

- (NSMutableDictionary *)headerParamters
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    return params;
}

@end
