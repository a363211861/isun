//
//  NetworkingClient.h
//  IOSSun
//
//  Created by Y W on 13-3-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AFHTTPClient.h"

@interface NetworkingClient : AFHTTPClient

+ (NetworkingClient *)sharedClient;
+ (NetworkingClient *)sharedUploadClient;
+ (NetworkingClient *)sharedDownloadClient;

-(AFHTTPRequestOperation *)getPath:(NSString *)path
                        parameters:(NSDictionary *)parameters
                           success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


-(AFHTTPRequestOperation *)postPath:(NSString *)path
                         parameters:(NSDictionary *)parameters
                            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(AFHTTPRequestOperation *)postUploadPath:(NSString *)path
                                dataArray:(NSArray *)dataArray
                               parameters:(NSDictionary *)parameters
                                  success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(AFHTTPRequestOperation *)getDownloadPath:(NSString *)path
                                  filePath:(NSString *)filePath
                                    append:(BOOL)append
                                 parameters:(NSDictionary *)parameters
                                   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(AFHTTPRequestOperation *)postDownloadPath:(NSString *)path
                                   filePath:(NSString *)filePath
                                     append:(BOOL)append
                                 parameters:(NSDictionary *)parameters
                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end