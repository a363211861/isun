//
//  AddInViewController.m
//  IOSSun
//
//  Created by Y W on 13-6-8.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AddInViewController.h"
#import "CellForTextField.h"
#import "UITextField+BBlock.h"
#import "UIView+BounceAnimation.h"

#import "SignInViewController.h"
#import "ChangePasswordViewController.h"

#import "User.h"


#define kTextFieldTag 1232

@interface AddInViewController ()

@property (nonatomic, strong)UIView *tableFooterView;

@end

@implementation AddInViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.needOtherButton = NO;
    }
    return self;
}

- (id)initPullToRefresh
{
    self = [super initPullToRefresh];
    if (self) {
        self.needOtherButton = NO;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.needOtherButton = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"注册";
    
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.tableFooterView == nil) {
        UIView *mView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 125)];
        mView.backgroundColor = [UIColor clearColor];
        self.tableFooterView = mView;
        
        int xOffset = 15, yOffset = 15, yDistance = 10, height = 30;
        {
            FUIButton *button = [[FUIButton alloc] initWithFrame:CGRectMake(xOffset, yOffset, self.tableView.bounds.size.width - 2 * xOffset, height)];
            button.backgroundColor = [UIColor clearColor];
            [button addTarget:self action:@selector(addInAction:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"注    册" forState:UIControlStateNormal];
            [mView addSubview:button];
            
            button.buttonColor = [UIColor turquoiseColor];
            button.shadowColor = [UIColor greenSeaColor];
            button.shadowHeight = 3.0f;
            button.cornerRadius = 6.0f;
            button.titleLabel.font = [UIFont boldFlatFontOfSize:15];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            
            yOffset += height + yDistance;
        }
        
        if (self.needOtherButton) {
            {
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                button.backgroundColor = [UIColor clearColor];
                button.frame = CGRectMake(xOffset, yOffset, self.tableView.bounds.size.width - 2 * xOffset, height);
                [button addTarget:self action:@selector(signInAction:) forControlEvents:UIControlEventTouchUpInside];
                [button setTitle:@"登    录" forState:UIControlStateNormal];
                [mView addSubview:button];
                
                yOffset += height + yDistance;
            }
            
            {
                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                button.backgroundColor = [UIColor clearColor];
                button.frame = CGRectMake(xOffset, yOffset, self.tableView.bounds.size.width - 2 * xOffset, height);
                [button addTarget:self action:@selector(changedPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
                [button setTitle:@"忘记密码" forState:UIControlStateNormal];
                [mView addSubview:button];
            }
        }
    }
    self.tableView.tableFooterView = self.tableFooterView;
}

#pragma mark - action
- (void)signInAction:(UIButton *)sender
{
    SignInViewController *signInViewController = [[SignInViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:signInViewController animated:YES];
}

- (void)changedPasswordAction:(UIButton *)sender
{
    ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] initWithStyle:UITableViewStyleGrouped];
    [self.navigationController pushViewController:changePasswordViewController animated:YES];
}

- (void)addInAction:(UIButton *)sender
{
    CellForTextField *userNameCell = (CellForTextField *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString *userName = [userNameCell.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (userName == nil || userName.length == 0) {
        [userNameCell.textField bounceStart];
        
        return;
    }
    
    CellForTextField *passwordCell = (CellForTextField *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    NSString *password = [passwordCell.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (password == nil || password.length == 0) {
        [passwordCell.textField bounceStart];
        
        return;
    } else if (password.length < 6) {
        [passwordCell.textField bounceStart];
        [self alert:@"请设置六位以上密码"];
        
        return;
    }
    
    CellForTextField *rePasswordCell = (CellForTextField *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    NSString *rePassword = [rePasswordCell.textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (rePassword == nil || rePassword.length == 0) {
        [rePasswordCell.textField bounceStart];
        
        return;
    } else if (![password isEqualToString:rePassword]) {
        
        [self alert:@"两次输入密码不相同"];
        
        return;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerSuccess:) name:kUserRegisterSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerFail:) name:kUserRegisterFail object:nil];
    
    User *user = [User sharedUser];
    user.userName = userName;
    user.password = password;
    [user userRegister];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier1 = @"Cell1";
    static NSString *cellIdentifier2 = @"Cell2";
    static NSString *cellIdentifier3 = @"Cell3";
    
    if (indexPath.row == 0) {
        CellForTextField *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        if (cell == nil) {
            cell = [[CellForTextField alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier1];
            cell.textField.placeholder = @"用户名";
            [cell.textField textFieldShouldReturnWithBlock:^BOOL(UITextField *textField) {
                [textField resignFirstResponder];
                return YES;
            }];
        }
        return cell;
    }
    
    if (indexPath.row == 1) {
        CellForTextField *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        if (cell == nil) {
            cell = [[CellForTextField alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier2];
            cell.textField.placeholder = @"密  码";
            [cell.textField textFieldShouldReturnWithBlock:^BOOL(UITextField *textField) {
                [textField resignFirstResponder];
                return YES;
            }];
        }
        return cell;
    }
    
    if (indexPath.row == 2) {
        CellForTextField *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        if (cell == nil) {
            cell = [[CellForTextField alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier3];
            cell.textField.placeholder = @"确认密码";
            [cell.textField textFieldShouldReturnWithBlock:^BOOL(UITextField *textField) {
                [textField resignFirstResponder];
                return YES;
            }];
        }
        return cell;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [CellForTextField cellHeight];
}

#pragma mark - Register networking result
- (void)registerSuccess:(NSNotification *)notification
{
    
}

- (void)registerFail:(NSNotification *)notification
{
    User *user = [notification object];
    if (user == [User sharedUser]) {
        NSDictionary *userInfo = [notification userInfo];
        NSError *error = [userInfo objectForKey:kUserNetworkingError];
        [self alert:error.localizedDescription];
    }
}

@end