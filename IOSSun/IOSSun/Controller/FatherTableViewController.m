//
//  FatherTableViewController.m
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherTableViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "Util.h"
#import "ISRefreshControl.h"

@interface FatherTableViewController () <EGORefreshTableHeaderDelegate>
{
    UITableViewStyle _tableViewStytle;
    
    BOOL _needPullToRefresh;
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}

@property (nonatomic, strong)UIRefreshControl *refreshControl;

@end

@implementation FatherTableViewController

- (id)init
{
    self = [super init];
    if (self) {
        _tableViewStytle = UITableViewStylePlain;
        _needPullToRefresh = NO;
    }
    return self;
}

- (id)initPullToRefresh
{
    self = [super init];
    if (self) {
        _tableViewStytle = UITableViewStylePlain;
        _needPullToRefresh = YES;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        _tableViewStytle = style;
        _needPullToRefresh = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:_tableViewStytle];
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.dataSource = self;
        tableView.delegate = self;
        [Util setExtraCellLineHidden:tableView];
        [self.view addSubview:tableView];
        _tableView = tableView;
    }
    
    if (_needPullToRefresh) {
        if ([[ApplicationManager defaultManager] systemVersion] < 6.0) {
            if (1) {
                EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
                view.delegate = self;
                [self.tableView addSubview:view];
                _refreshHeaderView = view;
            } else {
                ISRefreshControl *refreshControl = [[ISRefreshControl alloc] init];
                refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"下拉刷新"];
                [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
                [self.tableView addSubview:refreshControl];
                self.refreshControl = (id)refreshControl;
            }
        } else {
            UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
            refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"下拉刷新"];
            [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
            [self.tableView addSubview:refreshControl];
            self.refreshControl = refreshControl;
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

#pragma mark - ios systemVersion >= 6.0
- (void)handleRefresh:(UIRefreshControl *)refreshControl
{
    [self egoRefreshTableHeaderDidTriggerRefresh:nil];
}

#pragma mark - ios systemVersion <= 5.0
#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
}

- (void)doneLoadingTableViewData
{
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
	
    if (self.refreshControl) {
        [self.refreshControl endRefreshing];
        NSDate *date = [NSDate date];
		[NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehaviorDefault];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setAMSymbol:NSLocalizedString(@"上午", @"上午")];
		[dateFormatter setPMSymbol:NSLocalizedString(@"下午", @"下午")];
		[dateFormatter setDateFormat:@"yyyy/MM/dd hh:mm a"];
        NSString *dateString = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"最后更新", @"最后更新"), [dateFormatter stringFromDate:date]];
        self.refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:dateString];
    }
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    [self setNowRefershTime];
	[self refersh];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
	return _reloading; // should return if data source model is reloading	
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view
{
	return [NSDate date]; // should return date data source was last changed	
}

@end