//
//  MyCenterViewController.m
//  IOSSun
//
//  Created by Y W on 13-6-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "MyCenterViewController.h"

#import "UITableViewCell+FlatUI.h"
#import "UIColor+FlatUI.h"

#import "RosyWriterViewController.h"
#import "AVCamViewController.h"

@interface MyCenterViewController ()

@end

@implementation MyCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"个人中心";
    
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.tableView.separatorColor = self.view.backgroundColor;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = cell = [UITableViewCell configureFlatCellWithColor:[UIColor greenSeaColor] selectedColor:[UIColor cloudsColor] style:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.cornerRadius = 5.f; //Optional
        cell.separatorHeight = 2.f; //Optional
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = @"个人信息";
    }
    
    if (indexPath.section == 1) {
        cell.textLabel.text = @"我的视频";
    }
    
    if (indexPath.section == 2) {
        cell.textLabel.text = @"录制视频";
    }
    
    return cell;
}


#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 2) {
        if (0) {
            RosyWriterViewController *rosyWriterViewController = [[RosyWriterViewController alloc] init];
            [self.navigationController presentViewController:rosyWriterViewController animated:YES completion:nil];
        } else {
            AVCamViewController *avCamViewController = [[AVCamViewController alloc] init];
            [self.navigationController presentViewController:avCamViewController animated:YES completion:nil];
        }
        
    }
}
@end
