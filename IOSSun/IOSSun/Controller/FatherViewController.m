//
//  FatherViewController.m
//  IOSSun
//
//  Created by Y W on 13-3-24.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherViewController.h"

#import "TKAlertCenter.h"


#pragma mark - 页面刷新时间控制

@interface RefershTimeControl : NSObject

{
    NSMutableDictionary *_classAndTimeDictionary;
}
+ (RefershTimeControl *)sharedRefershTimeControl;

- (NSNumber *)getRefershTimeWithClassTimeMD5:(NSString *)classTimeMD5;
- (void)setRefershTime:(NSNumber *)refershTime classTimeMD5:(NSString *)classTimeMD5;

- (void)deleteRefershTimeWithClassTimeMD5:(NSString *)classTimeMD5;

@end



@implementation RefershTimeControl

+ (RefershTimeControl *)sharedRefershTimeControl
{
    static RefershTimeControl *sharedRefershTimeControl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRefershTimeControl = [[RefershTimeControl alloc] init];
    });
    return sharedRefershTimeControl;
}

- (id)init
{
    self = [super init];
    if (self) {
        _classAndTimeDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}


- (NSNumber *)getRefershTimeWithClassTimeMD5:(NSString *)classTimeMD5
{
    if (classTimeMD5 == nil) {
        return nil;
    }
    
    return [_classAndTimeDictionary objectForKey:classTimeMD5];
}

- (void)setRefershTime:(NSNumber *)refershTime classTimeMD5:(NSString *)classTimeMD5
{
    if (refershTime == nil || classTimeMD5 == nil) {
        return;
    }
    
    [_classAndTimeDictionary setObject:refershTime forKey:classTimeMD5];
}

- (void)deleteRefershTimeWithClassTimeMD5:(NSString *)classTimeMD5
{
    if (classTimeMD5 == nil) {
        return;
    }
    
    [_classAndTimeDictionary removeObjectForKey:classTimeMD5];
}

@end


#pragma mark - FatherViewController

@interface FatherViewController ()
{
    //alert
    TKAlertCenter *_alertCenter;
    
    //about network
    NSTimeInterval _needRefershSecs;
    BOOL _canRefersh;
}

@property (nonatomic, strong)NSString *classTimeMD5;
@property (nonatomic, weak)RefershTimeControl *refershControl;

@end


@implementation FatherViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cloudsColor];
    
    NSString *classTimeString = [NSString stringWithFormat:@"%@%@", [self class], [Util dateStringFromDate:[NSDate date] withType:1]];
    self.classTimeMD5 = [Util md5StringForString:classTimeString];
    self.refershControl = [RefershTimeControl sharedRefershTimeControl];
    
    _needRefershSecs = 60;
    _canRefersh = YES;
    [self setNeedRefershTime:_needRefershSecs];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
	self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.userInteractionEnabled = NO;
    
    NSNumber *timeNumber = [self.refershControl getRefershTimeWithClassTimeMD5:self.classTimeMD5];
    if (timeNumber == nil) {
        [self setNowRefershTime];
        [self refersh];
    } else {
        NSTimeInterval timeSecs = [timeNumber doubleValue];
        NSTimeInterval currentTimeSecs = [[NSDate date] timeIntervalSince1970];
        if (currentTimeSecs - timeSecs > _needRefershSecs) {
            [self setNowRefershTime];
            [self refersh];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
	self.navigationItem.rightBarButtonItem.enabled = YES;
    self.view.userInteractionEnabled = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    
	self.navigationItem.leftBarButtonItem.enabled = NO;
	self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.userInteractionEnabled = NO;
}

@end




@implementation FatherViewController (alert)

-(TKAlertCenter *)alertCenter
{
    if (_alertCenter == nil) {
        _alertCenter = [TKAlertCenter defaultCenter];
    }
    return _alertCenter;
}

-(void)alert:(NSString *)alertString
{
    if (alertString == nil || ![alertString isKindOfClass:[NSString class]]) {
        return;
    }
    [self.alertCenter postAlertWithMessage:alertString];
}

-(void)alert:(NSString *)alertString image:(UIImage*)image
{
    if (alertString == nil || ![alertString isKindOfClass:[NSString class]]) {
        return;
    }
    
    if (image == nil || ![image isKindOfClass:[UIImage class]]) {
        return;
    }
    [self.alertCenter postAlertWithMessage:alertString image:image];
}

@end



@implementation FatherViewController (network)

//refersh
-(void)setNeedRefershTime:(NSTimeInterval)secs
{
    if (secs < 0) {
        secs = 60;
    }
    _needRefershSecs = secs;
}


- (void)setNowRefershTime
{
    NSTimeInterval currentTimeSecs = [[NSDate date] timeIntervalSince1970];
    [self.refershControl setRefershTime:[NSNumber numberWithDouble:currentTimeSecs] classTimeMD5:self.classTimeMD5];
}


-(void)refersh
{
    
}

@end