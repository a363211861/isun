//
//  FatherViewController.h
//  IOSSun
//
//  Created by Y W on 13-3-24.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FlatUIKit.h"
#import "Util.h"

@interface FatherViewController : UIViewController

@end

@interface FatherViewController (alert)

- (void)alert:(NSString *)alertString image:(UIImage*)image;
- (void)alert:(NSString *)alertString;

@end



@interface FatherViewController (networkControl)

//viewWillAppear refersh
- (void)setNeedRefershTime:(NSTimeInterval)secs;
- (void)setNowRefershTime;
- (void)refersh;

@end