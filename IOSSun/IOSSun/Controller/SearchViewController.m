//
//  SearchViewController.m
//  IOSSun
//
//  Created by Y W on 13-6-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "SearchViewController.h"
#import "UIImage+Scale.h"

@interface CustomSearchBar : UISearchBar

@end

@implementation CustomSearchBar

- (void)addSubview:(UIView *)view
{
    [super addSubview:view];
    
    if ([view isKindOfClass:[UIButton class]]) {
        UIButton *button = (UIButton *)view;
        if ([button.titleLabel.text isEqualToString:@"取消"] || [button.titleLabel.text isEqualToString:@"cancel"]) {
            [button setBackgroundImage:[UIImage imageWithColor:[UIColor silverColor] cornerRadius:5] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageWithColor:[UIColor silverColor] cornerRadius:5] forState:UIControlStateHighlighted];
        }
    }
}

@end


@interface SearchViewController () <UISearchBarDelegate, UISearchDisplayDelegate>
{
    CustomSearchBar *_searchBar;
    UISearchDisplayController *_searchDisplayController;
}
@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"搜索";
    
    {
        CustomSearchBar *searchBar = [[CustomSearchBar alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 44)];
        searchBar.backgroundColor = self.view.backgroundColor;
        searchBar.tintColor = self.view.backgroundColor;
        searchBar.clipsToBounds = YES;
        searchBar.translucent = YES;
        searchBar.delegate = self;
        //1.
        [[searchBar.subviews objectAtIndex:0] removeFromSuperview];
        //2.
        for (UIView *subview in searchBar.subviews) {
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subview removeFromSuperview];
                break;
            }
        }
        {
            UIImage *image = [UIImage imageWithColor:[UIColor whiteColor] cornerRadius:5];
            image = [image OriginImage:image scaleToSize:CGRectInset(searchBar.bounds, 10, 5).size];
            [searchBar setSearchFieldBackgroundImage:[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2] forState:UIControlStateNormal];
        }
        self.tableView.tableHeaderView = searchBar;
        _searchBar = searchBar;
    }
    
    {
        UISearchDisplayController *searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:_searchBar contentsController:self];
        searchDisplayController.delegate = self;
        searchDisplayController.searchResultsDataSource = self;
        searchDisplayController.searchResultsDelegate = self;
        _searchDisplayController = searchDisplayController;
    }
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

@end
