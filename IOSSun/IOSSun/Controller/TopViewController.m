//
//  TopViewController.m
//  IOSSun
//
//  Created by Y W on 13-6-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "TopViewController.h"

@interface TopViewController ()
{
    UISegmentedControl *_segmentedControl;
}
@end

@implementation TopViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    FUISegmentedControl *segmentedControl = [[FUISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"今天", @"本周", @"全部", nil]];
    segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    segmentedControl.selectedFont = [UIFont boldFlatFontOfSize:14];
    segmentedControl.selectedFontColor = [UIColor whiteColor];
    segmentedControl.deselectedFont = [UIFont flatFontOfSize:14];
    segmentedControl.deselectedFontColor = [UIColor whiteColor];
    segmentedControl.selectedColor = [UIColor amethystColor];
    segmentedControl.deselectedColor = [UIColor silverColor];
    segmentedControl.dividerColor = [UIColor midnightBlueColor];
    segmentedControl.cornerRadius = 2.0;
    segmentedControl.selectedSegmentIndex = 0;
    [segmentedControl addTarget:self action:@selector(segmentedAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = segmentedControl;
    self.navigationItem.titleView.bounds = CGRectMake(0, 0, 200, 30);
    _segmentedControl = segmentedControl;
    [self segmentedAction:segmentedControl];
}

#pragma mark - action

- (void)segmentedAction:(UISegmentedControl *)segmentedControl
{
    
}

@end
