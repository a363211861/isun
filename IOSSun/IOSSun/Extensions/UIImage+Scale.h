//
//  UIImage+Scale.h
//  IOSSun
//
//  Created by Y W on 13-6-17.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Scale)

-(UIImage*)OriginImage:(UIImage *)image scaleToSize:(CGSize)size;

@end
