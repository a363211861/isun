//
//  AppDelegate.m
//  IOSSun
//
//  Created by Y W on 13-3-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "WWaitingBar.h"

#import "UINavigationBar+FlatUI.h"
#import "UITabBar+FlatUI.h"

//tabBar
#import "FeaturedViewController.h"
#import "CategoriesViewController.h"
#import "TopViewController.h"
#import "SearchViewController.h"
#import "MyCenterViewController.h"

//addIn & signIn
#import "CQMFloatingController.h"
#import "CQMFloatingNavigationBar.h"
#import "AddInViewController.h"
#import "SignInViewController.h"

#import "User.h"

#define kTabBarItemTag 1234

#define kStartNumber @"kStartNumber"

@interface AppDelegate () <CQMFloatingControllerDelegate>
{
    WWaitingBar *_waitingBar;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor midnightBlueColor];
    
    AFNetworkActivityIndicatorManager *afNetworkActivityIndicatorManager = [AFNetworkActivityIndicatorManager sharedManager];
    [afNetworkActivityIndicatorManager setEnabled:YES];
    
    [self addObserver:afNetworkActivityIndicatorManager forKeyPath:@"isNetworkActivityIndicatorVisible" options:NSKeyValueObservingOptionNew context:nil];
    
    [UIBarButtonItem configureFlatButtonsWithColor:[UIColor peterRiverColor]
                                  highlightedColor:[UIColor belizeHoleColor]
                                      cornerRadius:3
                                   whenContainedIn:[UINavigationBar class], nil];
    NSDictionary *barItemAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor cloudsColor],
                                UITextAttributeTextColor,
                                [UIColor whiteColor],
                                UITextAttributeTextShadowColor,
                                [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
                                UITextAttributeTextShadowOffset,
                                [UIFont boldFlatFontOfSize:14],
                                UITextAttributeFont,
                                nil];
    [[UIBarButtonItem appearance] setTitleTextAttributes:barItemAttributes forState:UIControlStateNormal];
    
    
    NSDictionary *navBarAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor cloudsColor],
                                UITextAttributeTextColor,
                                [UIColor whiteColor],
                                UITextAttributeTextShadowColor,
                                [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],
                                UITextAttributeTextShadowOffset,
                                [UIFont boldFlatFontOfSize:20],
                                UITextAttributeFont,
                                nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navBarAttributes];
    [[UINavigationBar appearance] configureFlatNavigationBarWithColor:[UIColor midnightBlueColor]];
    
    
    NSMutableArray *tabBarItems = [[NSMutableArray alloc] init];
    
    [tabBarItems addObject:[self getTabBarNavigationControllerItem:[[FeaturedViewController alloc] initPullToRefresh] title:@"精选" image:[UIImage imageNamed:@"featured.png"] finishedSelectedImage:nil finishedUnselectedImage:nil tag:kTabBarItemTag]];
    [tabBarItems addObject:[self getTabBarNavigationControllerItem:[[CategoriesViewController alloc] initPullToRefresh] title:@"分类" image:[UIImage imageNamed:@"categories.png"] finishedSelectedImage:nil finishedUnselectedImage:nil tag:kTabBarItemTag + 1]];
    [tabBarItems addObject:[self getTabBarNavigationControllerItem:[[TopViewController alloc] initPullToRefresh] title:@"热门" image:[UIImage imageNamed:@"top.png"] finishedSelectedImage:nil finishedUnselectedImage:nil tag:kTabBarItemTag + 2]];
    [tabBarItems addObject:[self getTabBarNavigationControllerItem:[[SearchViewController alloc] init] title:@"搜索" image:[UIImage imageNamed:@"search.png"] finishedSelectedImage:nil finishedUnselectedImage:nil tag:kTabBarItemTag + 3]];
    [tabBarItems addObject:[self getTabBarNavigationControllerItem:[[MyCenterViewController alloc] init] title:@"个人中心" image:[UIImage imageNamed:@"myCenter.png"] finishedSelectedImage:nil finishedUnselectedImage:nil tag:kTabBarItemTag + 4]];
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.delegate = self;
    [tabBarController.tabBar configureFlatTabBarWithColor:[UIColor midnightBlueColor] selectedColor:[UIColor turquoiseColor]];
    [tabBarController setViewControllers:tabBarItems animated:NO];
    tabBarController.view.frame = self.window.bounds;
    
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger startNumber = [userDefaults integerForKey:kStartNumber];
    startNumber++;
    [userDefaults setInteger:startNumber forKey:kStartNumber];
    [userDefaults synchronize];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - Functions
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"isNetworkActivityIndicatorVisible"]) {
        if (_waitingBar == nil) {
            _waitingBar = [[WWaitingBar alloc] initWithRadius:25.0 color:kRGBColor(25, 110, 206, 1) timeInterval:0.2];
            _waitingBar.hidden = YES;
            _waitingBar.backgroundColor = [UIColor clearColor];
            CGPoint center = [[UIApplication sharedApplication] keyWindow].center;
            center.x += _waitingBar.frame.size.width / 2;
            center.y += _waitingBar.frame.size.height / 2;
            _waitingBar.center = center;
            [[[UIApplication sharedApplication] keyWindow] addSubview:_waitingBar];
        }
        _waitingBar.hidden = ![AFNetworkActivityIndicatorManager sharedManager].isNetworkActivityIndicatorVisible;
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (UINavigationController *)getTabBarNavigationControllerItem:(UIViewController *)viewController title:(NSString *)title image:(UIImage *)image finishedSelectedImage:(UIImage *)selectedImage finishedUnselectedImage:(UIImage *)unselectedImage tag:(NSInteger)tag
{
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:image tag:tag];
    [tabBarItem setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:unselectedImage];
    navigationController.tabBarItem = tabBarItem;
    return navigationController;
}

- (void)signIn
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger startNumber = [userDefaults integerForKey:kStartNumber];

    CQMFloatingController *floatingController = [CQMFloatingController sharedFloatingController];
    floatingController.delegate = self;
    [floatingController setFrameColor:[UIColor cloudsColor]];
    
    UIViewController *viewController = nil;
    if (startNumber == 1) {
        AddInViewController *addInViewController = [[AddInViewController alloc] initWithStyle:UITableViewStyleGrouped];
        addInViewController.needOtherButton = YES;
        viewController = addInViewController;
    } else {
        SignInViewController *signInViewController = [[SignInViewController alloc] initWithStyle:UITableViewStyleGrouped];
        signInViewController.needOtherButton = YES;
        viewController = signInViewController;
    }
    [floatingController presentWithContentViewController:viewController animated:YES];
}


#pragma mark - notification
- (void)signInSuccess:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserSignInSuccess object:nil];
    
    CQMFloatingController *floatingController = [CQMFloatingController sharedFloatingController];
    [floatingController dismissAnimated:YES];
    
    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    [tabBarController setSelectedIndex:4];
}

#pragma mark - UITabBarControllerDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if (tabBarController == self.window.rootViewController) {
        if ((viewController.tabBarItem.tag == kTabBarItemTag + 4) && ![[User sharedUser] token]) {
            
            [self signIn];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInSuccess:) name:kUserSignInSuccess object:nil];
            
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - CQMFloatingControllerDelegate
- (void)CQMFloatingControllerViewWillAppear:(CQMFloatingController *)floatingController
{
}

- (void)CQMFloatingControllerViewDisAppear:(CQMFloatingController *)floatingController
{
}
@end
