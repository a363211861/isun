//
//  User.m
//  IOSSun
//
//  Created by Y W on 13-7-5.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "User.h"
#import "Util.h"
#import "Networking.h"

#define kUserName @"kUserName"
#define kUserPassWord @"kUserPassWord"

@interface User () <NetworkingDelegate>

@property(nonatomic, strong)Networking *registerNetworking;
@property(nonatomic, strong)Networking *signInNetworking;

@end

@implementation User

+ (User *)sharedUser
{
    static User *sharedUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [[User alloc] init];
    });
    return sharedUser;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        _userName = [userDefaults objectForKey:kUserName];
        _password = [userDefaults objectForKey:kUserPassWord];
    }
    return self;
}

- (void)dealloc
{
    [self userSignOut];
}

- (void)setAttributes:(NSDictionary *)attributes
{
    
}

//setter and getter
- (void)setUserName:(NSString *)userName
{
    _userName = userName;
    if (userName == nil || [userName isEqualToString:@""]) {
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:userName forKey:kUserName];
    [userDefaults synchronize];
}

- (void)setPassword:(NSString *)password
{
    _password = password;
    if (password == nil || [password isEqualToString:@""]) {
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:password forKey:kUserPassWord];
    [userDefaults synchronize];
}


//注册
- (void)userRegister
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"ISunRegister" forKey:@"method"];
    [parameters setObject:self.userName forKey:@"userName"];
    [parameters setObject:[self getMD5ByPassword:self.password] forKey:@"password"];
    
    self.registerNetworking = [[Networking alloc] initPostParameters:parameters delegate:self];
}

//登录
- (void)userSignIn
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"ISunLogin" forKey:@"method"];
    [parameters setObject:self.userName forKey:@"userName"];
    [parameters setObject:[self getMD5ByPassword:self.password] forKey:@"password"];
    
    self.signInNetworking = [[Networking alloc] initPostParameters:parameters delegate:self];
}

//注销
- (void)userSignOut
{
    self.userName = nil;
    self.password = nil;
    _token = nil;
    self.registerNetworking.delegate = nil;
    self.registerNetworking = nil;
    self.signInNetworking.delegate = nil;
    self.signInNetworking = nil;
}

#pragma mark - NetworkingDelegate
- (void)networkingSuccess:(Networking *)networking
{
    if (networking && networking == self.registerNetworking) {
        NSDictionary *responseDic = networking.responseObject;
        NSDictionary *resultDic = [responseDic objectForKey:@"result"];
        
        assert([resultDic isKindOfClass:[NSDictionary class]]);
        
        id token = [resultDic objectForKey:@"token"];
        
        assert(token);
        assert([token isKindOfClass:[NSString class]]);
        
        _token = token;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserRegisterSuccess object:self];
    }
    
    if (networking && networking == self.signInNetworking) {
        NSDictionary *responseDic = networking.responseObject;
        NSDictionary *resultDic = [responseDic objectForKey:@"result"];
        
        assert([resultDic isKindOfClass:[NSDictionary class]]);
        
        id token = [resultDic objectForKey:@"token"];
        
        assert(token);
        assert([token isKindOfClass:[NSString class]]);
        
        _token = token;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserSignInSuccess object:self];
    }
}

- (void)networkingFail:(Networking *)networking
{
    if (networking && networking == self.registerNetworking) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserRegisterFail object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:networking.error, kUserNetworkingError, nil]];
    }
    
    if (networking && networking == self.signInNetworking) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kUserSignInFail object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:networking.error, kUserNetworkingError, nil]];
    }
}


#pragma mark - functions
- (NSString *)getMD5ByPassword:(NSString *)password
{
    if (password == nil || password.length == 0) {
        return nil;
    }
    password = [password stringByAppendingString:@"08*(!@#$#*$(#*!NKJSDD}XNSJRAI OSNF    NDSSDFdfa*(@!~你（）*&……%#@@~！@——++——"];
    return [Util md5StringForString:password];
}

@end
