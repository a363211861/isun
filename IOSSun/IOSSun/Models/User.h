//
//  User.h
//  IOSSun
//
//  Created by Y W on 13-7-5.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUserRegisterSuccess @"kUserRegisterSuccess"
#define kUserRegisterFail    @"kUserRegisterFail"

#define kUserSignInSuccess   @"kUserSignInSuccess"
#define kUserSignInFail      @"kUserSignInFail"

#define kUserNetworkingError @"kUserNetworkingError"

@interface User : NSObject

+(User *)sharedUser;

@property(nonatomic, strong)NSString *userName;
@property(nonatomic, strong)NSString *password;
@property(nonatomic, strong)NSString *email;

@property(nonatomic, readonly)NSString *token;

- (void)setAttributes:(NSDictionary *)attributes;

//注册
- (void)userRegister;

//登录
- (void)userSignIn;

//注销
- (void)userSignOut;

@end
