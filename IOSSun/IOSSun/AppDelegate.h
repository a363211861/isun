//
//  AppDelegate.h
//  IOSSun
//
//  Created by Y W on 13-3-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
