//
//  Util.h
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

//生成MD5签名
+ (NSString *)md5StringForString:(NSString *)string;

//判断是不是电话号码
+ (BOOL)isMobileNumber:(NSString *)mobileNum;

//判断是不是固定电话
+ (BOOL)isTellPhoneNumber:(NSString *)tellNumber;

//判断是不是邮箱地址
+ (BOOL)isEmail:(NSString *)candidate;

//去掉UITableview多余的分割线
+ (void)setExtraCellLineHidden:(UITableView *)tableView;

//根据date获取字符串
//type == 1 返回格式 yyyy-MM-dd HH:mm:ss
//type == 2 返回格式 yyyy-MM-dd
//type == 3 返回格式 yyyyMMdd
+(NSString *)dateStringFromDate:(NSDate *)date withType:(int)type;

//将日期字符串转换为nsdate
//type == 1 返回格式 yyyy-MM-dd HH:mm:ss
//type == 2 返回格式 yyyy-MM-dd
//type == 3 返回格式 yyyyMMdd
+(NSDate *)dateFromDateString:(NSString *)dateString withType:(int)type;

+(UIImage *)viewToImage:(UIView *)view;

//生成一个本地化描述为LocalizedDescription的NSError
+ (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription;
@end
