//
//  Util.m
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Util.h"
#import <CommonCrypto/CommonDigest.h>
#import <QuartzCore/QuartzCore.h>

@implementation Util

+ (NSString *)md5StringForString:(NSString *)string
{
    const char *str = [string UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), r);
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
}



+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

+ (BOOL)isTellPhoneNumber:(NSString *)tellNumber
{
    NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    NSPredicate *regextestphs = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    return [regextestphs evaluateWithObject:tellNumber];
}

+ (BOOL)isEmail:(NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (void)setExtraCellLineHidden:(UITableView *)tableView
{
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

+ (NSString *)dateStringFromDate:(NSDate *)date withType:(int)type
{
    if (date == nil) {
        return nil;
    }
    
    NSString *dateFormat = nil;
    switch (type) {
        case 1:
            dateFormat = @"yyyy-MM-dd HH:mm:ss";
            break;
        case 2:
            dateFormat = @"yyyy-MM-dd";
            break;
        case 3:
            dateFormat = @"yyyyMMdd";
            break;
        default:
            dateFormat = @"yyyy-MM-dd HH:mm:ss";
            break;
    }
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setLocale:[NSLocale currentLocale]];
    [dateFormater setDateFormat:dateFormat];
    
    return [dateFormater stringFromDate:date];
}


+ (NSDate *)dateFromDateString:(NSString *)dateString withType:(int)type;
{
    if (dateString == nil) {
        return nil;
    }
    
    NSString *dateFormat = nil;
    switch (type) {
        case 1:
            dateFormat = @"yyyy-MM-dd HH:mm:ss";
            break;
        case 2:
            dateFormat = @"yyyy-MM-dd";
            break;
        case 3:
            dateFormat = @"yyyyMMdd";
            break;
        default:
            dateFormat = @"yyyy-MM-dd HH:mm:ss";
            break;
    }
    
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setLocale:[NSLocale currentLocale]];
    [dateFormater setDateFormat:dateFormat];
    NSDate *date = [dateFormater dateFromString:dateString];
    return date;
}

+(UIImage *)viewToImage:(UIView *)view {
    UIGraphicsBeginImageContext(view.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription
{
    if (localizedDescription == nil || ![localizedDescription isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:localizedDescription, NSLocalizedDescriptionKey, nil];
    return [NSError errorWithDomain:localizedDescription code:-1 userInfo:userInfo];
}
@end
