//
//  ApplicationManager.m
//  IOSSun
//
//  Created by Y W on 13-4-7.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "ApplicationManager.h"
#import "Util.h"

@implementation ApplicationManager

+ (ApplicationManager *)defaultManager
{
    static ApplicationManager *defaultManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultManager = [[ApplicationManager alloc] init];
    });
    return defaultManager;
}

- (float)systemVersion
{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}

- (NSString *)applicationVerSion
{
    return @"1.0.0";
}

@end
