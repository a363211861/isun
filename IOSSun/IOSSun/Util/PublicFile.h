//
//  PublicFile.h
//  IOSSun
//
//  Created by Y W on 13-4-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#ifndef IOSSun_PublicFile_h
#define IOSSun_PublicFile_h

//真机去掉log
#ifdef __i386__
#    define NSLog(...) NSLog(__VA_ARGS__)
#else
#    define NSLog(...) {}
#endif

#define kRGBColor(R, G, B, a) [UIColor colorWithRed:(R)/255.0 green:(G)/255.0 blue:(B)/255.0 alpha:(a)]

#endif
