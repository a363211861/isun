//
//  ApplicationManager.h
//  IOSSun
//
//  Created by Y W on 13-4-7.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationManager : NSObject

+ (ApplicationManager *)defaultManager;

- (float)systemVersion;
- (NSString *)applicationVerSion;

@end
