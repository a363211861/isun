//
//  Waiting.h
//  Nav
//
//  Created by frank on 11-6-30.
//  Copyright 2011 wongf70@gmail.com All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface WWaitingBar : UIView {
	int ballNum;
	int timeCount;
	NSArray*animationArray;
}
- (id)initWithRadius:(float)aRadius color:(UIColor*)aColor timeInterval:(float)aInterval;
@end
