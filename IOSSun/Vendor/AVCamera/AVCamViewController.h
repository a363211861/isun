//
//  AVCamViewController.h
//  TestAVCam
//
//  Created by Y W on 13-7-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCamCaptureManager, AVCamPreviewView, AudioButton, AVCaptureVideoPreviewLayer;

@interface AVCamViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic,retain) AVCamCaptureManager *captureManager;
@property (nonatomic,retain) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;

@property (nonatomic,retain) UIView *videoPreviewView;
@property (nonatomic,retain) UIButton *cameraToggleButton;
@property (nonatomic,retain) AudioButton *recordButton;
@property (nonatomic,retain) UIButton *stillButton;
@property (nonatomic,retain) UILabel *focusModeLabel;

#pragma mark Toolbar Actions
- (void)toggleRecording:(id)sender;
- (void)captureStillImage:(id)sender;
- (void)toggleCamera:(id)sender;

@end
